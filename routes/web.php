<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/user', UserController::class);
Route::POST('login',[AuthController::class,'login'])->name('user.login');
Route::POST('register',[AuthController::class,'register'])->name('user.register');
Route::GET('list',[UserController::class,'list'])->name('user.list');