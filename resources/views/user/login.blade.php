@extends('layout.master')
@section('content')
<style>

    #caixa{
        
        width:500px;
        align-items: center;
        border: solid #000 1px;
        border-radius: 5px;     
        padding: 25px 25px;
        margin: 100px 250px  0px 325px;
        box-sizing: border-box;
    }
    #caixa > input{
        text-align: center;
    }
</style>
    @if(session('warning'))
        @alert
            {{session('warning')}}
        @endalert
    @endif
    <div id="caixa">
        <form action="{{route('user.login')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="exampleInputPassword1">Email:</label>
                <input name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Senha:</label>
                <input name="password" type="password" class="form-control"  placeholder="Password">
            </div> 
            <input type="submit" value="Entrar" class="btn btn-primary">
            <a href="{{route('user.create')}}" class="btn btn-outline-success">Cadastro</a>
        </form>
    </div>
@endsection