<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6'
        ];
    }
    public function messages(){
        
        return[
            'name.required' => 'O campo nome é obrigatório.',
            'email.required' => 'O campo email é obrigatório.',
            'email.email'=>'Email inválido',
            'email.unique' => 'Email já foi cadastrado no sistema.',
            'password.required' => 'O campo senha é obrigatório.',
            'password.min' => 'A senha tem que conter no mininimo 6 carecteres.',
            'password.confirmed' => 'As senhas não se coincidem.',
        ];
    }
}
