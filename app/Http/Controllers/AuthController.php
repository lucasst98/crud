<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Registration;
use App\Http\Controllers\Controller;
use App\Models\Register;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\AuthRegisterRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AuthLoginRequest;


class AuthController extends Controller
{
    public function register(AuthRegisterRequest $request)
    {
        $validated = $request->validated();
    
        $user = User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => bcrypt($validated['password']),
        ]);
        
        return view ('user.login');
    }

    public function login(AuthLoginRequest $request)
    {
        
        $validated = $request->validated();
        
        if(Auth::attempt($validated)){
            return redirect()->route('user.list');
        }else
            return [
                'message' => 'Email ou senha inválido'
            ];
    }
}